#!/bin/bash

set -euxo pipefail

export GNUPGHOME=`mktemp -d`
echo "Using temporary directory $GNUPGHOME"

export TEST_DATA_DIR=`dirname $0`

gpg --import $TEST_DATA_DIR/*.asc

export ARCHIE=05F597E88FAD0763449F8D1F573FD879821C2735
export BOB=097722C2A2EF2E7AFB2D0C345902E7D5FB4E1ECD
export CHARLIE=1351878A47640D0812452E5057546E564D259DBB
export DANIEL=FE9BE6F2F92C4A3B536D326FCC3160C3C54E50BB
export EVE=4E27CF523A3880CD4FD3B4532D147A3EE202DBBE

gpg -u $ARCHIE \
    --sig-notation thoroughness@tmp.metacode.biz=2 \
    --sig-notation understanding@tmp.metacode.biz=4 \
    --sig-notation rating@tmp.metacode.biz=9000 \
    --detach-sign $TEST_DATA_DIR/file.txt

mv $TEST_DATA_DIR/file.txt.sig $GNUPGHOME/file-archie.sig
gpg --export $ARCHIE > $GNUPGHOME/archie.pgp

# Bob is not required by policy.json

gpg -u $CHARLIE \
    --sig-notation thoroughness@tmp.metacode.biz=2 \
    --sig-notation understanding@tmp.metacode.biz=4 \
    --sig-notation rating@tmp.metacode.biz=9000 \
    --detach-sign $TEST_DATA_DIR/file.txt

mv $TEST_DATA_DIR/file.txt.sig $GNUPGHOME/file-charlie.sig
gpg --export $CHARLIE > $GNUPGHOME/charlie.pgp

gpg -u $DANIEL \
    --sig-notation thoroughness@tmp.metacode.biz=2 \
    --sig-notation understanding@tmp.metacode.biz=4 \
    --sig-notation rating@tmp.metacode.biz=9000 \
    --detach-sign $TEST_DATA_DIR/file.txt

mv $TEST_DATA_DIR/file.txt.sig $GNUPGHOME/file-daniel.sig
gpg --export $DANIEL > $GNUPGHOME/daniel.pgp

gpg -u $EVE \
    --sig-notation thoroughness@tmp.metacode.biz=2 \
    --sig-notation understanding@tmp.metacode.biz=4 \
    --sig-notation rating@tmp.metacode.biz=9000 \
    --detach-sign $TEST_DATA_DIR/file.txt

mv $TEST_DATA_DIR/file.txt.sig $GNUPGHOME/file-eve.sig
gpg --export $EVE > $GNUPGHOME/eve.pgp

cat $GNUPGHOME/archie.pgp $GNUPGHOME/charlie.pgp $GNUPGHOME/daniel.pgp $GNUPGHOME/eve.pgp > $GNUPGHOME/keyring.pgp
cat $GNUPGHOME/file-archie.sig $GNUPGHOME/file-charlie.sig $GNUPGHOME/file-daniel.sig $GNUPGHOME/file-eve.sig > $GNUPGHOME/sigring.pgp

../target/debug/lance-verifier $GNUPGHOME/sigring.pgp $TEST_DATA_DIR/file.txt $GNUPGHOME/keyring.pgp $TEST_DATA_DIR/policy.json

for policy in $TEST_DATA_DIR/failing-policies/*.json; do

  echo "Verifying failing policy $policy..."

  if ../target/debug/lance-verifier $GNUPGHOME/sigring.pgp $TEST_DATA_DIR/file.txt $GNUPGHOME/keyring.pgp $policy; then
    echo "The verification succeeded but should fail!"
    exit 1
  else
    echo "OK"
  fi

done

echo "All tests succeeded!"
