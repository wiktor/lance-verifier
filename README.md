# Lance Verifier

Verifies signatures against rich requirements policy.

For example assume that you have a file that should be signed by several different groups of people (represented by their certificates) with stringent rules on the number of required signatures and metadata associated with them.

This tool does just that.

For it to do its work it needs just a couple of things:

  1. Keyring file - that is a concatenated list of all binary certificates that it will work with,
  2. Sigring file - concatenated list of all detached signatures over the file. Since this file is a pure concatenation it's easy to assemble it over time,
  3. Policy document - a JSON formatted file describing rules the signatures need to obey,
  4. The file to verify.

## Signing

To compute the signature with attached metadata use:

```sh
gpg -u $KEY \
    --sig-notation thoroughness@tmp.metacode.biz=2 \
    --sig-notation understanding@tmp.metacode.biz=4 \
    --sig-notation rating@tmp.metacode.biz=9000 \
    --detach-sign file.txt
```

The exact names of `thoroughness@tmp.metacode.biz=2` are not relevant but must match the names in the policy file.
As per RFC 4880 suggestion they should use an e-mail-like syntax so that it's clear which party controls the notation's semantics.

## Policies

The policy document describes how many signatures are required and which notations must be present.

For example the following document requires at least one signature from `release-engineers` group and three from the `engineers` group.
Each of these signatures must contain given signatures:

```json
[
    {
        "name": "release-engineers",
        "min": 1,
        "members": [
            "05F597E88FAD0763449F8D1F573FD879821C2735",
            "097722C2A2EF2E7AFB2D0C345902E7D5FB4E1ECD"
        ],
        "metadata": {
            "thoroughness@tmp.metacode.biz": {
                "min": 2
            },
            "understanding@tmp.metacode.biz": {
                "min": 4
            },
            "rating@tmp.metacode.biz": {
                "present": true
            }
        }
    },
    {
        "name": "engineers",
        "min": 3,
        "members": [
            "1351878A47640D0812452E5057546E564D259DBB",
            "FE9BE6F2F92C4A3B536D326FCC3160C3C54E50BB",
            "4E27CF523A3880CD4FD3B4532D147A3EE202DBBE"
        ],
        "metadata": {
            "thoroughness@tmp.metacode.biz": {
                "min": 1
            },
            "understanding@tmp.metacode.biz": {
                "min": 2
            },
            "rating@tmp.metacode.biz": {
                "present": true
            }
        }
    }
]
```

## Test data

For more elaborate examples including signing the data in place see `test-data/test-all.sh` file.

There are also several test cases for the un-happy-path in the `test-data/failing-policies` directory.

## Q&A

### Why tmp.metacode.biz?

The notation suffix does not matter for the verification engine but one is needed to conform to RFC 4880.
If you use this project please replace `tmp.metacode.biz` with your domain name.

### Why keyring?

The certificates need to be supplied to the application in one way or another.
Keyring/certring is simple to create and operate on.

Do note that the keyring is *not* trusted in any way.
(This is in contrast to the "curated keyring" schemes).
