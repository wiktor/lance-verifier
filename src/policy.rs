use std::collections::HashMap;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct PolicyRequirements(Vec<PolicyRequirement>);

#[derive(Deserialize, Debug)]
pub struct PolicyRequirement {
    pub name: String,
    pub min: usize,
    pub members: Vec<String>,
    pub metadata: HashMap<String, RequirementMetadataValue>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum RequirementMetadataValue {
    Min(usize),
    Present(bool),
}
