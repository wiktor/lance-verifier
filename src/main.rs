use std::io::BufReader;

use lance_verifier::openpgp::verify_file;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = std::env::args().collect::<Vec<_>>();

    let results = verify_file(&args[3], &args[1], &args[2])?;

    let policy: Vec<lance_verifier::policy::PolicyRequirement> =
        serde_json::from_reader(BufReader::new(std::fs::File::open(&args[4])?))?;

    lance_verifier::verifier::verify(results, policy)?;

    Ok(())
}
