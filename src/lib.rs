pub mod openpgp;
pub mod policy;
pub mod verifier;

#[derive(Debug)]
pub struct ValidSigResult {
    pub fingerprint: String,
    pub notations: Vec<(String, String)>,
}
