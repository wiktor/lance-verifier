use super::*;
use openpgp::cert::prelude::*;
use openpgp::parse::{stream::*, Parse};
use openpgp::policy::StandardPolicy;
use sequoia_openpgp as openpgp;
use std::path::Path;

pub fn verify_file<P, U, S>(keyring: P, sigring: U, file: S) -> openpgp::Result<Vec<ValidSigResult>>
where
    P: AsRef<Path>,
    U: AsRef<Path>,
    S: AsRef<Path>,
{
    let mut certs = vec![];
    let parser = CertParser::from_file(keyring.as_ref())?;
    certs.extend(parser.flatten());

    let policy = &StandardPolicy::new();

    let helper = Helper {
        certs: &certs,
        results: vec![],
    };

    let mut verifier =
        DetachedVerifierBuilder::from_file(sigring.as_ref())?.with_policy(policy, None, helper)?;

    verifier.verify_file(file.as_ref())?;
    let helper = verifier.into_helper();
    Ok(helper.results)
}

struct Helper<'a> {
    certs: &'a [openpgp::Cert],
    results: Vec<ValidSigResult>,
}

impl<'a> VerificationHelper for Helper<'a> {
    fn get_certs(&mut self, _ids: &[openpgp::KeyHandle]) -> openpgp::Result<Vec<openpgp::Cert>> {
        Ok(self.certs.into())
    }

    fn check(&mut self, structure: MessageStructure) -> openpgp::Result<()> {
        for (i, layer) in structure.into_iter().enumerate() {
            match (i, layer) {
                (0, MessageLayer::SignatureGroup { results }) => {
                    if results.is_empty() {
                        return Err(anyhow::anyhow!("No signature"));
                    }
                    for sig in results {
                        match sig {
                            Ok(res) => {
                                let notations = res
                                    .sig
                                    .notation_data()
                                    .filter(|data| data.flags().human_readable())
                                    .map(|data| {
                                        (
                                            data.name().into(),
                                            String::from_utf8_lossy(data.value()).to_string(),
                                        )
                                    })
                                    .collect::<Vec<_>>();
                                self.results.push(ValidSigResult {
                                    fingerprint: res.ka.cert().fingerprint().to_hex(),
                                    notations,
                                });
                            }
                            Err(e) => return Err(openpgp::Error::from(e).into()),
                        }
                    }
                }
                _ => return Err(anyhow::anyhow!("Unexpected message structure")),
            }
        }

        Ok(())
    }
}
