use super::policy::{PolicyRequirement, RequirementMetadataValue};
use super::ValidSigResult;

#[derive(Debug)]
pub struct VerificationError {
    pub group: String,
    pub requirement: RequirementError,
}

#[derive(Debug)]
pub enum RequirementError {
    MinMembers { actual: usize, expected: usize },
    NotationMissing { name: String },
    NotationPresent { name: String },
    NotationMin { actual: usize, expected: usize },
}

impl std::fmt::Display for VerificationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for VerificationError {}

pub fn verify(
    results: Vec<ValidSigResult>,
    requirements: Vec<PolicyRequirement>,
) -> Result<(), VerificationError> {
    for requirement in requirements {
        let members_matched = results
            .iter()
            .filter(|result| requirement.members.contains(&result.fingerprint))
            .count();
        if members_matched < requirement.min {
            return Err(VerificationError {
                group: requirement.name,
                requirement: RequirementError::MinMembers {
                    actual: members_matched,
                    expected: requirement.min,
                },
            });
        }
        for (key, value_requirement) in requirement.metadata.iter() {
            results
                .iter()
                .filter(|result| requirement.members.contains(&result.fingerprint))
                .try_for_each(|result| {
                    let notations = result
                        .notations
                        .iter()
                        .filter(|(notation_key, _)| notation_key == key)
                        .collect::<Vec<_>>();
                    if notations.is_empty() {
                        return Err(VerificationError {
                            group: requirement.name.clone(),
                            requirement: RequirementError::NotationMissing { name: key.clone() },
                        });
                    }
                    for (_, value) in notations.into_iter() {
                        match value_requirement {
                            RequirementMetadataValue::Min(min) => {
                                if let Ok(actual) = value.parse::<usize>() {
                                    if actual < *min {
                                        return Err(VerificationError {
                                            group: requirement.name.clone(),
                                            requirement: RequirementError::NotationMin {
                                                actual,
                                                expected: *min,
                                            },
                                        });
                                    }
                                } else {
                                    todo!();
                                }
                            }
                            RequirementMetadataValue::Present(present) => {
                                if !present {
                                    return Err(VerificationError {
                                        group: requirement.name.clone(),
                                        requirement: RequirementError::NotationPresent {
                                            name: key.clone(),
                                        },
                                    });
                                }
                            }
                        }
                    }
                    Ok(())
                })?;
        }
    }
    Ok(())
}
